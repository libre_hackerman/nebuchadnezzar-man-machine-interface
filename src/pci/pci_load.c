/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <pci.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

/*
 * This function returns a heap allocated string with the full path to the
 * /sys/devices/pci* folder associated with the passed PCI bus. It returns
 * NULL on error.
 */
static char*
get_pci_dir(const char* pci_bus)
{
	FILE* f_find;
	char command[43];
	char* pci_dir;
	size_t getline_n;

	if (strlen(pci_bus) != 7)
	{
		fprintf(stderr, "%s is not a valid PCI bus\n", pci_bus);
		return (NULL);
	}
	sprintf(command, "find /sys/devices -type d -name \"*%s\"", pci_bus);
	if (!(f_find = popen(command, "r")))
	{
		perror(command);
		return (NULL);
	}

	pci_dir = NULL;
	getline_n = 0;
	if (getline(&pci_dir, &getline_n, f_find) == -1)
	{
		free(pci_dir);
		pclose(f_find);
		fprintf(stderr, "%s is not a valid PCI bus\n", pci_bus);
		return (NULL);
	}

	pclose(f_find);
	pci_dir[strlen(pci_dir) - 1] = '\0'; /* Remove ending \n */
	return (pci_dir);
}

/*
 * This function returns the number of files contained in a given directory.
 */
static size_t
count_files(const char* path)
{
	DIR* d;
	size_t count;

	if (!(d = opendir(path)))
	{
		perror(path);
		return (0);
	}

	count = 0;
	while (readdir(d))
		count++;

	closedir(d);
	return (count);
}

/*
 * This function reads the PCI bus associated with the given DRI directory
 * and stores it into bus_buffer (which must be a buffer of at least size 8).
 */
static void
get_dri_bus(const char* path, char* bus_buffer)
{
	FILE* f;
	char* name;

	name = calloc(strlen(path) + 6, sizeof(char));
	sprintf(name, "%s/name", path);
	if (!(f = fopen(name, "r")))
	{
		perror(name);
		free(name);
		bus_buffer[0] = '\0';
		return;
	}

	fscanf(f, "amdgpu dev=0000:%7s unique=%*s\n", bus_buffer);
	fclose(f);
	free(name);
}

static char*
get_dri_dir(const char* pci_bus)
{
	DIR* d;
	struct dirent* dirent;
	char candidate_bus[8];
	char* candidate;

	if (!(d = opendir("/sys/kernel/debug/dri")))
	{
		perror("/sys/kernel/debug/dri");
		return (NULL);
	}

	candidate = calloc(strlen("/sys/kernel/debug/dri/") + 4, sizeof(char));
	while ((dirent = readdir(d)))
	{
		sprintf(candidate, "/sys/kernel/debug/dri/%s", dirent->d_name); 
		if (count_files(candidate) < 50)
			continue;
		get_dri_bus(candidate, candidate_bus);
		if (strcmp(candidate_bus, pci_bus) == 0)
		{
			closedir(d);
			return (candidate);
		}
	}

	fprintf(stderr, "Couldn't find DRI directory for PCI %s\n", pci_bus);
	free(candidate);
	closedir(d);
	return (NULL);
}

/*
 * This function returns a heap allocated struct with the path for the sysfs
 * PCI directory and its associated DRI directory given the PCI bus of a GPU.
 * Returns NULL on error.
 */
struct pci_s*
pci_load(const char* pci_bus)
{
	struct pci_s* pci;

	pci = malloc(sizeof(struct pci_s));
	pci->pci_dir = get_pci_dir(pci_bus);
	pci->dri_dir = get_dri_dir(pci_bus);

	if (!pci->pci_dir || !pci->dri_dir)
	{
		pci_delete(pci);
		return (NULL);
	}

	return (pci);
}
