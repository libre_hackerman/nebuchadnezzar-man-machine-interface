/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <utils.h>

#include <stdio.h>
#include <stdlib.h>

/*
 * This function is like sprintf but instead of writing to a passed string,
 * it allocates one on the heap and returns it. So, like a formatted strdup.
 */
char*
strdupf(const char* format, ...)
{
	va_list ap;
	char* dup;

	va_start(ap, format);
	dup = calloc(vsnprintf(NULL, 0, format, ap) + 1, sizeof(char));
	va_end(ap);

	va_start(ap, format);
	vsprintf(dup, format, ap);
	va_end(ap);

	return (dup);
}
