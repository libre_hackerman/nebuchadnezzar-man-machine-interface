/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <utils.h>

#include <sys/stat.h>
#include <linux/limits.h>
#include <unistd.h>
#include <stdlib.h>

/*
 * Returns true if the passed path is an executable file.
 */
bool
is_executable(const char* path)
{
	struct stat sb;
	char* link;
	bool executable;

	if (stat(path, &sb) != 0)
		return (false);

	/* Follow symlinks */
	if (S_ISLNK(sb.st_mode))
	{
		link = calloc(PATH_MAX, sizeof(char));
		readlink(path, link, PATH_MAX);
		executable = is_executable(link);
		free(link);
		return (executable);
	}

	if (!S_ISREG(sb.st_mode))
		return (false);

	return (sb.st_mode & (S_IXUSR | S_IXGRP | S_IXOTH));
}
