/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <stdlib.h>

/*
 * This function deallocates the memory used by a struct config_s.
 */
void
config_delete(struct config_s* config)
{
	if (config)
	{
		free(config->pool);
		free(config->pool_protocol);
		free(config->wallet);
		free(config->worker_name);
		free(config->miner_user);
		free(config->logs_dir);
		free(config->ethminer_path);
		free(config->amdmemtweak_path);
		free(config);
	}
}
