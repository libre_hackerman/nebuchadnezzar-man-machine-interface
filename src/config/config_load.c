/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <utils.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <ctype.h>

/*
 * This function initializes all fields of a struct config_s to invalid default
 * values (char* to NULL and int to -1).
 */
static void
config_initialize(struct config_s* config)
{
	memset(config, 0, sizeof(struct config_s));
	config->secs_per_frame = -1;
	config->shader_min_profile = -1;
	config->shader_max_profile = -1;
	config->shader_speed = -1;
	config->shader_voltage = -1;
	config->memory_min_profile = -1;
	config->memory_max_profile = -1;
	config->memory_speed = -1;
	config->memory_voltage = -1;
	config->ref = -1;
	config->fans = -1;
}

/*
 * This function returns a heap allocated string with the identifier of the
 * passed configuration line. If the line doesn't have a identifier delimiter
 * (a colon) it returns NULL.
 */
static char*
config_get_identifier(const char* line)
{
	char* identifier;
	char* colon;

	identifier = strdup(line);
	if ((colon = strchr(identifier, ':')))
	{
		*colon = '\0';
		return (identifier);
	}
	else
	{
		free(identifier);
		return (NULL);
	}
}

/*
 * This function retuns a heap allocated string with the value of the passed
 * config line.
 */
static char*
config_parse_str_value(const char* line)
{
	return (strdup(strchr(line, ':') + 1));
}

/*
 * This function returns an unsigned integer with the value of the passed
 * config file, or -1 if its not a valid number.
 */
static int
config_parse_int_value(const char* line)
{
	char* value;

	value = strchr(line, ':') + 1;
	return (isdigit(value[0]) ? atoi(value) : -1);
}

/*
 * This function checks if any of the fields of the struct is set to an invalid
 * default value, in which case it would return false;
 */
static bool
config_check_values(struct config_s* config)
{
	/* Check strings */
	if (!config->pool || !config->pool_protocol || !config->wallet ||
			!config->worker_name || !config->logs_dir || !config->miner_user ||
			!config->ethminer_path || !config->amdmemtweak_path)
		return (false);
	/* Check integers */
	if (config->secs_per_frame == -1 || config->shader_min_profile == -1 ||
			config->shader_max_profile == -1 || config->shader_speed == -1 ||
			config->shader_voltage == -1 || config->memory_min_profile == -1 ||
			config->memory_max_profile == -1 || config->memory_speed == -1 ||
			config->memory_voltage == -1 || config->ref == -1 ||
			config->fans == -1)
		return (false);

	return (true);
}

/*
 * This function reads a configuration file and writes its values in the passed
 * struct. It will allocate and store a string with a short description of
 * the operation result to output_str.
 * Returns false in case of error.
 */
static bool
config_read_fields(const char* filename, struct config_s* config,
		char** output_str)
{
	FILE* f;
	size_t getline_n;
	char* line;
	char* identifier;

	if (!(f = fopen(filename, "r")))
	{
		*output_str = strdupf("%s: %s", filename, strerror(errno));
		return (false);
	}

	line = NULL;
	getline_n = 0;
	while (getline(&line, &getline_n, f) != -1)
	{
		remove_ending_newline(line);
		if (!(identifier = config_get_identifier(line)))
		{
			*output_str = strdupf("Invalid configuration line: %s", line);
			free(line);
			fclose(f);
			return (false);
		}

		if (strcmp(identifier, "POOL") == 0)
			config->pool = config_parse_str_value(line);
		else if (strcmp(identifier, "PROTOCOL") == 0)
			config->pool_protocol = config_parse_str_value(line);
		else if (strcmp(identifier, "WALLET") == 0)
			config->wallet = config_parse_str_value(line);
		else if (strcmp(identifier, "WORKER") == 0)
			config->worker_name = config_parse_str_value(line);
		else if (strcmp(identifier, "MINER_USER") == 0)
			config->miner_user = config_parse_str_value(line);
		else if (strcmp(identifier, "LOGSDIR") == 0)
			config->logs_dir = config_parse_str_value(line);
		else if (strcmp(identifier, "ETHMINER") == 0)
			config->ethminer_path = config_parse_str_value(line);
		else if (strcmp(identifier, "AMDMEMTWEAK") == 0)
			config->amdmemtweak_path = config_parse_str_value(line);
		else if (strcmp(identifier, "SECS_FRAME") == 0)
			config->secs_per_frame = config_parse_int_value(line);
		else if (strcmp(identifier, "SH_MIN_PROFILE") == 0)
			config->shader_min_profile = config_parse_int_value(line);
		else if (strcmp(identifier, "SH_MAX_PROFILE") == 0)
			config->shader_max_profile = config_parse_int_value(line);
		else if (strcmp(identifier, "SH_SPEED") == 0)
			config->shader_speed = config_parse_int_value(line);
		else if (strcmp(identifier, "SH_VOLTAGE") == 0)
			config->shader_voltage = config_parse_int_value(line);
		else if (strcmp(identifier, "MEM_MIN_PROFILE") == 0)
			config->memory_min_profile = config_parse_int_value(line);
		else if (strcmp(identifier, "MEM_MAX_PROFILE") == 0)
			config->memory_max_profile = config_parse_int_value(line);
		else if (strcmp(identifier, "MEM_SPEED") == 0)
			config->memory_speed = config_parse_int_value(line);
		else if (strcmp(identifier, "MEM_VOLTAGE") == 0)
			config->memory_voltage = config_parse_int_value(line);
		else if (strcmp(identifier, "REF") == 0)
			config->ref = config_parse_int_value(line);
		else if (strcmp(identifier, "FANS_RPM") == 0)
			config->fans = config_parse_int_value(line);
		else
		{
			*output_str = strdupf("Invalid configuration identifier: %s",
					identifier);
			free(identifier);
			free(line);
			fclose(f);
			return (false);
		}

		free(identifier);
		free(line);
		line = NULL;
		getline_n = 0;
	}

	free(line);
	fclose(f);
	if (config_check_values(config))
	{
		*output_str = strdup("Sucess");
		return (true);
	}
	else
	{
		*output_str = strdup("Invalid configuration");
		return (false);
	}
}

/*
 * This function reads a configuration file and returns a heap allocated
 * struct with its information. It will allocate and store a string with a
 * short description of the operation result to output_str.
 * Returns NULL in case of error.
 */
struct config_s*
config_load(const char* filename, char** output_str)
{
	struct config_s* config;

	config = malloc(sizeof(struct config_s));
	config_initialize(config);

	if (!config_read_fields(filename, config, output_str))
	{
		config_delete(config);
		return (NULL);
	}

	return (config);
}
