/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <utils.h>

#include <stdio.h>
#include <string.h>
#include <errno.h>

/*
 * This function writes the passed config struct to the specified file,
 * returning true on success. It will allocate and store an string with a
 * short description of the operation result to output_str.
 */
bool
config_write(const char* filename, const struct config_s* config,
		char** output_str)
{
	FILE* f;

	if (!(f = fopen(filename, "w")))
	{
		*output_str = strdupf("%s: %s", filename, strerror(errno));
		return (false);
	}

	fprintf(f, "POOL:%s\n", config->pool);
	fprintf(f, "PROTOCOL:%s\n", config->pool_protocol);
	fprintf(f, "WALLET:%s\n", config->wallet);
	fprintf(f, "WORKER:%s\n", config->worker_name);
	fprintf(f, "MINER_USER:%s\n", config->miner_user);
	fprintf(f, "LOGSDIR:%s\n", config->logs_dir);
	fprintf(f, "ETHMINER:%s\n", config->ethminer_path);
	fprintf(f, "AMDMEMTWEAK:%s\n", config->amdmemtweak_path);
	fprintf(f, "SECS_FRAME:%u\n", config->secs_per_frame);
	fprintf(f, "SH_MIN_PROFILE:%u\n", config->shader_min_profile);
	fprintf(f, "SH_MAX_PROFILE:%u\n", config->shader_max_profile);
	fprintf(f, "SH_SPEED:%u\n", config->shader_speed);
	fprintf(f, "SH_VOLTAGE:%u\n", config->shader_voltage);
	fprintf(f, "MEM_MIN_PROFILE:%u\n", config->memory_min_profile);
	fprintf(f, "MEM_MAX_PROFILE:%u\n", config->memory_max_profile);
	fprintf(f, "MEM_SPEED:%u\n", config->memory_speed);
	fprintf(f, "MEM_VOLTAGE:%u\n", config->memory_voltage);
	fprintf(f, "REF:%u\n", config->ref);
	fprintf(f, "FANS:%u\n", config->fans);

	fclose(f);
	*output_str = strdup("Success");
	return (true);
}
