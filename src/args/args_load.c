/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <args.h>

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*
 * This function allocates a struct args_s and initializes to the default
 * values.
 */
static struct args_s*
init_args()
{
	struct args_s* args;

	args = malloc(sizeof(struct args_s));
	args->load_config = NULL;
	args->gpu_bus = NULL;
	args->secs_frame = 2;

	return (args);
}

/*
 * Displays the help, duh
 */
static void
display_help(const char* exec_name)
{
	int opt_width = 31;

	printf("Usage: %s [-l FILE] [-p FILE] [-g BUS] [-f FRAMES] [-h]\n",
			exec_name);
	puts("\nThis is a Ncurses manager for Ethminer and AMD GPUs");
	puts("\nOptions:");
	printf("%-*sLaunch the program with the given configuration"
			" >>MANDATORY<<\n",	opt_width, "-l, --load-config <file>");
	printf("%-*sSet the PCI bus of the GPU >>MANDATORY<<\n",
			opt_width, "-g, --gpu <bus>");
	printf("%-*sSet the framerate of the interface every 10 seconds\n",
			opt_width, "-s, --secs-frame <secs/frame>");
	printf("%-*sDisplay the config file help\n", opt_width, "-c, --help-config");
	printf("%-*sDisplay this help\n", opt_width, "-h, --help");
}

static void
display_help_config()
{
	puts("The program requires a configuration file in order to set the mining"
			" pool and GPU settings");
	puts("The syntax of the file is one setting per line, like:");
	puts("CONFIG1:VALUE\nCONFIG2:VALUE\nCONFIG3:VALUE\n...");
	puts("\nThese are the configurations that must be set in the file:");
	puts("PROTOCOL:<string> (the protocol of the pool)");
	puts("WALLET:<string>");
	puts("WORKER:<string> (name of the worker for the pool)");
	puts("LOGSDIR:<string> (path to the directory where to store the logs)");
	puts("SECS_FRAME:<number> (how many seconds between each TUI reload)");
	puts("SH_MIN_PROFILE:<number> (ID of the minimum shader profile to which"
			" apply speed/voltage configuration)");
	puts("SH_MAX_PROFILE:<number> (ID of the maximum shader profile to which"
			" apply speed/voltage configuration)");
	puts("SH_SPEED:<number> (Speed in MHz to set the shader profiles to)");
	puts("SH_VOLTAGE:<number> (Voltage in mV to set the shader profiles to)");
	puts("MEM_MIN_PROFILE:<number> (ID of the minimum memory profile to which"
			" apply speed/voltage configuration)");
	puts("MEM_MAX_PROFILE:<number> (ID of the maximum memory profile to which"
			" apply speed/voltage configuration)");
	puts("MEM_SPEED:<number> (Speed in MHz to set the memory profiles to)");
	puts("MEM_VOLTAGE:<number> (Voltage in mV to set the memory profiles to)");
	puts("REF:<number>");
	puts("FANS:<number> (percentage to which to set the fans at)");
}

/*
 * This function receives the CLI arguments and parses them returning a heap
 * allocated struct args_s with the parsed data or NULL in case of error.
 */
struct args_s*
args_load(int argc, char* argv[])
{
	int op;
	struct args_s* args;
	struct option long_options[] = {
		{"load-config", required_argument, NULL, 'l'},
		{"gpu", required_argument, NULL, 'g'},
		{"secs-frame", required_argument, NULL, 's'},
		{"help-config", no_argument, NULL, 'c'},
		{"help", no_argument, NULL, 'h'},
		{0, 0, 0, 0}
	};

	args = init_args();
	while ((op = getopt_long(argc, argv, ":l:g:s:ch", long_options, NULL)) != -1)
	{
		switch (op)
		{
			case 'l':
				free(args->load_config);
				args->load_config = strdup(optarg);
				break;
			case 'g':
				free(args->gpu_bus);
				args->gpu_bus = strdup(optarg);
				break;
			case 's':
				if (!isdigit(optarg[0]))
				{
					fprintf(stderr, "Invalid secs/frame: %s\n", optarg);
					args_delete(args);
					return (NULL);
				}
				args->secs_frame = atoi(optarg);
				break;
			case 'h':
				display_help(argv[0]);
				args_delete(args);
				exit(0);
				break;
			case 'c':
				display_help_config();
				args_delete(args);
				exit(0);
				break;
			case ':':
				fputs("Missing required argument\n", stderr);
				args_delete(args);
				return (NULL);
			case '?':
				fputs("Unknown CLI argument\n", stderr);
				args_delete(args);
				return (NULL);
		}
	}

	if (!args->load_config)
	{
		fputs("Missing config file (--load-config)\n", stderr);
		args_delete(args);
		return (NULL);
	}
	if (!args->gpu_bus)
	{
		fputs("Missing GPU bus (--gpu)\n", stderr);
		args_delete(args);
		return (NULL);
	}
	return (args);
}
