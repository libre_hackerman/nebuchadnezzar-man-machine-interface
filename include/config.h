/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <stdbool.h>

struct config_s
{
	char* pool
	char* pool_protocol;
	char* wallet;
	char* worker_name;
	char* miner_user;

	char* logs_dir;

	char* ethminer_path;
	char* amdmemtweak_path;

	int secs_per_frame;

	int shader_min_profile;
	int shader_max_profile;
	int shader_speed;
	int shader_voltage;

	int memory_min_profile;
	int memory_max_profile;
	int memory_speed;
	int memory_voltage;

	int ref;

	int fans;
};

struct config_s*
config_load(const char* filename, char** output_str);

bool
config_write(const char* filename, const struct config_s* config,
		char** output_str);

void
config_delete(struct config_s* config);

#endif
