/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GPU_H
#define GPU_H

#include <pci.h>

#include <stdbool.h>
#include <stdlib.h>

struct profile_slot_s
{
	unsigned int speed;
	unsigned int voltage;
};

struct profiles_s
{
	struct profile_slot_s shader_profiles[8];
	struct profile_slot_s memory_profiles[3];

	unsigned int sel_shader_profile;
	unsigned int sel_memory_profile;
};

struct gpu_state_s
{
	unsigned int speed_shader;
	unsigned int speed_memory;
	unsigned int voltage;
	unsigned int power;
	unsigned int temperature;
	unsigned int fan_percentage;
	unsigned int fan_rpm;
	unsigned int gpu_load;
	unsigned int mem_load;
	unsigned int ref;
};

struct profiles_s*
gpu_load_profiles(const struct pci_s* pci, char** output_str);

bool
gpu_write_profiles(const struct pci_s* pci, const struct profiles_s* profiles,
		char** output_str);

struct gpu_state_s*
gpu_read_state(const struct pci_s* pci, const char* amdmemtweak_path,
		char** output_str);

#define gpu_delete_profiles(p) free(p)
#define gpu_delete_gpu_state(s) free(s)

#endif
